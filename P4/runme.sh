#! /bin/bash

what="myIOcalc"
thenasm="./nasm-2.13.03/nasm"
"${thenasm}" -f elf64 "${what}.asm" && gcc -o "${what}" "${what}.o"
gcc "${what}.c" -o "${what}C" -std=c11 -Wall -Wextra -Werror -O2 -Wno-unused-result

objdump -s -t -d -M intel "${what}" > "${what}.disasm.txt"
objdump -s -t -d -M intel "${what}C" > "${what}C.disasm.txt"
