// Diana Bell
// Project 4 extra credit
// TV20651.c

// gcc TV20651.c -o TV20651 -std=c11 -Wall -Wextra -Werror -O2 -Wno-unused-result

#include <stdio.h>
#include <limits.h>

const char eol[] = {0xA,0xD};

int unsigned long a, b, sum; // variables

int main() {
	
	printf("Enter number: "); // First number
	scanf("%lu", &a);

	printf("Enter number: "); // Second number
	scanf("%lu", &b);

	sum = a + b; // Add them

	if (sum > UINT_MAX) { // check for overflow
		printf("Sum is: OVF"); // Print overflow
		printf(eol); // New line
	}

	else {
		printf("Sum is: %lu", sum); // Print sum
		printf(eol); // New line
	}
	
	// End program
	return 0;
}
