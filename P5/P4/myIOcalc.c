// Diana Bell
// Project 4 (and extra credit)
// TV20651.c

// gcc TV20651.c -o TV20651 -std=c11 -Wall -Wextra -Werror -O2 -Wno-unused-result

#include <stdio.h>

const char eol[] = {0xA,0xD};

int a, b, sum; // variables

int main() {
	
	printf("Enter number: "); // First number
	scanf("%d", &a);

	printf("Enter number: "); // Second number
	scanf("%d", &b);

	sum = a + b; // Add them

	printf("Sum is: %d", sum); // Print sum
	printf(eol); // New line
	
	// End program
	return 0;
}
