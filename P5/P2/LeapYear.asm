;LeapYear.asm
;Diana Bell
;CMSC 313 Project 2

section .data ;Initialized data
prompt db "Enter year: "
prompt_len equ $ - prompt
notLeap db " is not a leap year."
notLeap_len equ $ - notLeap
isLeap db " is a leap year."
isLeap_len equ $ - isLeap
eol db 0xA, 0xD
eol_len equ $ - eol

section .bss ;Uninitialized data
year resd 1
yearInt resd 1 ;store year as int

section .text
global _start

print_int: ;Print result at the end
mov eax, 4 
mov ebx, 1
int 0x80
ret

_start: ;entry point
;Output prompt
mov eax, 4
mov ebx, 1
mov ecx, prompt
mov edx, prompt_len
int 0x80

;Take year input
mov eax, 3
mov ebx, 0
mov ecx, year
mov edx, 4
int 0x80

;convert digits
mov eax, [year]
and eax, 0xff
sub eax, '0'
mov ebx, 10
mul ebx
mov [yearInt], eax

mov eax, [year+1]
and eax, 0xff
sub eax, '0'
add eax, [yearInt]
mov ebx, 10
mul ebx
mov [yearInt], eax

mov eax, [year+2]
and eax, 0xff
sub eax, '0'
add eax, [yearInt]
mov ebx, 10
mul ebx
mov [yearInt], eax

mov eax, [year+3]
and eax, 0xff
sub eax, '0'
add eax, [yearInt]
mov ebx, 10
mul ebx
mov [yearInt], eax

;Determine if the year is divisible by 4
mov edx, 0
mov eax, [yearInt]
mov ebx, 4h

div ebx
cmp edx, 0x0
jne not_leap

hundredTest: ;Test if the year is divisible by 100
mov edx, 0
mov eax, [yearInt]
mov ebx, 64h ;100 in hex
div ebx
mov eax, edx
cmp eax, 0
je fourHundTest ;Test if it's divisible by 400
jne is_leap ;It's a leap year. Print appropriate output

fourHundTest: ;Test if the year is divisible by 400
mov edx, 0
mov eax, [yearInt]
mov ebx, 190h ;400 in hex
div ebx
mov eax, edx
cmp eax, 0
je is_leap ;Is divisiable by 100 and 400. It's a leap year
jne not_leap ;Is not divisible by 400. It's not a leap year 

not_leap: ;Execute if year is not a leap year
mov ecx, year
mov edx, 4
call print_int
mov ecx, notLeap
mov edx, notLeap_len
call print_int
mov ecx, eol
mov edx, eol_len
call print_int
jmp Exit

is_leap: ;Execute if year is a leap year
mov ecx, year
mov edx, 4
call print_int
mov ecx, isLeap
mov edx, isLeap_len
call print_int
mov ecx, eol
mov edx, eol_len
call print_int
jmp Exit

Exit: ;Exit program
mov eax, 1
xor ebx, ebx
int 0x80
