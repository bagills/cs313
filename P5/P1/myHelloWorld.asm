section .data 			;section declaration
msg db "Hello, world!",0xA 	;our string
msg2 db "I <3 CMSC 313",0xA	;second string
len equ $ - msg 		;length of our string
len2 equ $ - msg2		;length of second string
section .text 			;section declaration
global _start 			;we must export the entry point to the ELF linker or loader.
; They conventionally recognize _start as their entry point.
; Use ld -e foo to override the default.
_start:
;write our string to stdout (monitor)
mov eax,4 			;system call number (sys_write)
mov ebx,1 			;first argument: file handle (stdout)
mov ecx,msg 			;second argument: pointer to message to write
mov edx,len 			;third argument: message length			
int 0x80 			;call kernel and exit
mov ecx,msg2			;second argument: pointer to message2 to write
mov edx,len2			;third argument: message2 length
int 0x80			;call kernel and exit

;final exit
mov eax,1 			;system call number (sys_exit)
xor ebx,ebx 			;first syscall argument: exit code
int 0x80 			;call kernel to take over
