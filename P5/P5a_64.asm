; Diana Bell
; Project 5a - P5a_64.asm
; nasm -g -f elf64 P5a_64.asm && gcc -g -m64 -o P5a_64 P5a_64.o

extern printf
extern strlen

section .data
	msg	db	"Testing:",0xA,0x0
	len	equ	$ - msg
	msg2	db	"The length is: ",0x0
	len2	equ	$ - msg2
	fmt	db	"%s",0xA,0x0
	lfmt	equ	$ -fmt
	fmt2	db	"%u",0xA,0x0
	lfmt2	equ	$ - fmt2

	t1	db	"This is a test.",0x0
	l1	equ	$ - t1
	t2	db	"Newline test.",0xA,0x0
	l2	equ	$ - t2
	t3	db	0xA,0x0
	l3	equ	$ - t3
	t4	db	0x0
	l4	equ	$ - t4

section .bss
	ta		resd	4
	retv	resd	1

section .text
global main

main:
	push	rbp			; Save stack frame pointer
	mov	rbp, rsp		; Setup a stack frame

	; Setup tests
	mov	dword [ta],		t1	; 0
	mov	dword [ta+4],	t2	; 1
	mov	dword [ta+8],	t3	; 2
	mov	dword [ta+12],	t4	; 3
	mov	ecx, 3

loop:
	mov	esi, [ta+ecx*4]  	; Start at test 4
	push rcx				; Save RCX from potential clobbering

	; printf(msg)
	mov rdi, msg 			; Pass first parameter
	mov rax, 0 				; Empty rax 
	push rsi				; Preserve rsi
	call printf				; Call the function
	pop rsi					; Restore rsi

	; printf("%s\n",ta)
	mov rdi, fmt 			; Pass format, rsi is already set
	mov rax, 0				; Empty rax
	push rsi
	call printf				; Call the function
	pop rsi
	
	; retv = strlen(ta)
	mov rdi, rsi			; Pass first parameter
	call strlen				; Call the function
	mov	[retv],	rax			; Save return value

	; printf(msg2)
	mov rdi, msg2 			; Pass parameter
	mov rax, 0				; Empty rax
	call printf				; Call the function

	; printf("%u\n",retv)
	mov rdi, fmt2  			; Pass format
	push rsi 				; Preserve rsi
	mov rsi, [retv] 		; Pass return val
	mov rax, 0 				; Empty rax
	call printf				; Call the function
	pop rsi					; Restore rsi

	pop	rcx 				; Restore counter
	dec	rcx					; Decrement counter
	jns	loop				; Jump while non-negative

	pop	rbp					; Restore stack frame pointer

	mov	eax, 0				; main() return value
	ret