// Diana Bell
// P5b_functions.c
// Project 5 part B

/* Globals
struct element * dynarray;					// Dyn array
size_t allocated;						// Number of currently allocated elements
size_t head_idx;						// Index of first free element
*/

// DO NOT ADD/CHANGE ANYTHING OUTSIDE OF THE FUNCTION BODIES

// DO NOT CHANGE THE SIGNATURES
int putnumber(const char * buf) {
	unsigned long int ret;
	char *ptr;

	ret = strtoul(buf, &ptr, 10); // convert to unsigned long int, 0 if conversion fails

	if (ret == 0) { // if conversion failed
		return EXIT_FAILURE;
	}
	else { 
		dynarray[head_idx].ui = ret; // update member data
		dynarray[head_idx].str = NULL; // not a string
		head_idx += 1; // increment index	

		if (head_idx == allocated) { // if array is full
			allocated += 10; // increment allocated
			dynarray = realloc(dynarray, (allocated * sizeof(struct element))); // reallocate	
			if (dynarray == NULL) // check for failure
				return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}
}

int putstring(const char * buf) {
	int size = strlen(buf); 
	char *string;
	string = (char*) malloc(size+1); // allocate exact size of buf
	
	if (string == NULL) // if allocation failed
		return EXIT_FAILURE;

	else {
		strcpy(string, buf); // copy string over
		dynarray[head_idx].str = string; //set values
		dynarray[head_idx].ui = 0;
		head_idx += 1; // increment index

		if (head_idx == allocated) { // if array is full
			allocated += 10; // increment allocated
			dynarray = realloc(dynarray, (allocated * sizeof(struct element))); // reallocate		
			if (dynarray == NULL) // check for failure
				return EXIT_FAILURE;
		}
		return EXIT_SUCCESS;
	}
}

void dealloc() {
	for (int i = 0; i < (int)head_idx; ++i) {
		free(dynarray[i].str);
	}
}
