// gcc ./P5b.c -o ./P5b -std=c11 -Wall -Wextra -Werror -O2 -Wno-unused-result
// valgrind --leak-check=full --track-origins=yes --show-reachable=yes ./P5b ./inputfile.txt 0 14 22 32

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>

#define BUFF_SIZE	255					// Each input line is guaranteed to be no more than this (including newline and NUL)

struct element {						// Element of the dyn array
	unsigned long int ui;
	char * str;
};

// Globals
struct element * dynarray = NULL;				// Dyn array
size_t allocated = 0;						// Number of currently allocated elements
size_t head_idx = 0;						// Index of first free element

int putnumber(const char * buf);
int putstring(const char * buf);
void dealloc();

#include "P5b_functions.c"

int main(int argc, char** argv) {
	if (argc < 3) {
		printf("Insufficient arguments. "
			"You need to provide an input file "
			"and at least one index (all indexes "
			"need to exist in the input file, "
			"first index is 0). Usage examples:\n"
			"P5b inputfile.txt 4\n"
			"P5b inputfile.txt 0 32\n"
			"P5b inputfile.txt 1 9 33\n");
		return EXIT_FAILURE;
	}

	// Initialize dynamic array
	allocated = 10;
	dynarray = calloc(allocated, sizeof (struct element));
	if (NULL == dynarray) {
		fprintf(stderr, "Could not allocate memory.");
		return EXIT_FAILURE;
	}
	head_idx = 0;

	FILE * input = fopen(argv[1], "r");			// Open input file
	if (NULL == input) {					// If error
		perror("Could not open input file.");
		return EXIT_FAILURE;
	}

	char buffer[BUFF_SIZE];					// String buffer (temp variable)
	int i = 0;
	while (NULL != fgets(buffer, BUFF_SIZE-1, input)) {	// Read line, or until error or EOF
		buffer[strcspn(buffer, "\n")] = '\0';		// Replace newline with NUL
		if (0 != isdigit((int) (buffer[0]))) {		// Is a number
			if (EXIT_SUCCESS != putnumber(buffer)) {
				fclose(input);
				return EXIT_FAILURE;
			}
		}
		else {						// Is a string
			if (EXIT_SUCCESS != putstring(buffer)) {
				fclose(input);
				return EXIT_FAILURE;
			}
		}
		i++;
	}
	// NULL means error or EOF
	if (0 == feof(input)) {					// 0 means no EOF, so probably error
		if (0 != ferror(input)) {			// Error confirmed
			perror("Could not read input file.");
			fclose(input);
			return EXIT_FAILURE;
		}
		else {
			fprintf(stderr, "Unexplained non-error reading from input file.");
			fclose(input);
			return EXIT_FAILURE;
		}
	}
	else {							// EOF, so close input file
		fclose(input);
	}

	for (size_t j = 3; j <= (size_t) argc; j++) {		// Print desired indexes
		// Robust ASCII to integer conversion
		unsigned long int v1;
		errno = 0;					// Clear errno
		v1 = strtoul(argv[j-1], NULL, 10);		// Attempt conversion
		if ((v1 == ULONG_MAX && errno == ERANGE) || \
			v1 > 4294967295) {			// If error, abort
			fprintf(stderr, "Input does not fit "
				"in the predetermined range: "
				"0-4294967295. Aborting.\n");
			return EXIT_FAILURE;
		}

		if (NULL != dynarray[v1].str) {			// If str is non-null
			printf("%s\n",dynarray[v1].str);	// Print str
		}
		else {
			printf("%lu\n",dynarray[v1].ui);	// Print ui
		}
	}

	dealloc();						// Start deallocating
	allocated = 0;
	free(dynarray);						// Final dealloc
	head_idx = 0;
	return EXIT_SUCCESS;
}