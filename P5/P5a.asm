; nasm -f elf32 P5a.asm && gcc -m32 -o P5a P5a.o

extern printf
extern strlen

section .data
	msg	db	"Testing:",0xA,0x0
	len	equ	$ - msg
	msg2	db	"The length is: ",0x0
	len2	equ	$ - msg2
	fmt	db	"%s",0xA,0x0
	lfmt	equ	$ -fmt
	fmt2	db	"%u",0xA,0x0
	lfmt2	equ	$ - fmt2

	t1	db	"This is a test.",0x0
	l1	equ	$ - t1
	t2	db	"Newline test.",0xA,0x0
	l2	equ	$ - t2
	t3	db	0xA,0x0
	l3	equ	$ - t3
	t4	db	0x0
	l4	equ	$ - t4

section .bss
	ta	resd	4
	retv	resd	1

section .text
global main

main:
	push	ebp			; Save stack frame pointer
	mov	ebp,	esp		; Setup a stack frame

	; Setup tests
	mov	dword [ta],	t1	; 0
	mov	dword [ta+4],	t2	; 1
	mov	dword [ta+8],	t3	; 2
	mov	dword [ta+12],	t4	; 3
	mov	ecx, 	3

loop:
	mov	esi,	[ta+ecx*4]
	push	ecx			; Save ECX from potential clobbering

	; printf(msg)
	push	dword msg		; Push the argument into the stack
	call	printf			; Call the function
	add	esp,	byte 4		; Clean the stack

	; printf("%s\n",ta)
	push	esi			; Push the argument into the stack
	push	dword fmt		; Push the argument into the stack
	call	printf			; Call the function
	add	esp,	byte 8		; Clean the stack
	
	; retv = strlen(ta)
	push	esi			; Push the argument into the stack
	call	strlen			; Call the function
	add	esp,	byte 4		; Clean the stack
	mov	[retv],	eax		; Save return value

	; printf(msg2)
	push	dword msg2		; Push the argument into the stack
	call	printf			; Call the function
	add	esp,	byte 4		; Clean the stack

	; printf("%u\n",retv)
	push	dword [retv]		; Push the argument into the stack
	push	dword fmt2		; Push the argument into the stack
	call	printf			; Call the function
	add	esp,	byte 8		; Clean the stack

	pop	ecx
	dec	ecx			; Decrement counter
	jns	loop			; Jump while non-negative

	mov	esp,	ebp		; Undo stack frame
	pop	ebp			; Restore stack frame pointer

	mov	eax, 0			; main() return value
	ret
