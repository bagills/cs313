; Diana Bell
; tv20651.asm
; CMSC 313 Project 3 - TicTacSweeper

; Syscall numbers
%define SYS_EXIT 1
%define SYS_READ 3
%define SYS_WRITE 4

%define STDOUT 1
%define STDIN 0

%define DIAGONALS_WIN

; Expect 3..maxBoardSize. Anything else should cause the program to abort
%define WRONG_BOARD_SIZE_ABORTS

%define MAX_BOARD_SIZE		5
%define IO_BUFFER_SIZE		20

%define PLAYER_X			1
%define PLAYER_O			2

%define CELL_X				PLAYER_X
%define CELL_O				PLAYER_O
%define CELL_MINE(player)	(player << 4)
%define CELL_MINE_X			CELL_MINE(CELL_X)
%define CELL_MINE_O			CELL_MINE(CELL_O)

; New line and carriage return
%define NEWLINE				13,10

; Macro to define a string and its length in one line
%macro string 2+
	%1 db %2
	%{1}_length equ $ - %1
	%{1}_end db 0
%endmacro

%macro easy_print 1
	mov		ecx, %1
	mov		edx, %{1}_length
	call	print_int
%endmacro


section .data

; Used by debugging functions
HEX db "0123456789ABCDEF"

string sExitingProgram, "Exiting the program",NEWLINE
string sProgramHasExited, "The program has exited",NEWLINE

string sEnableDebug, "Do you want to enable debug information? "
string sBoardSize, "Board size (3..5): "
string sWrongBoardSize, "Wrong input, expected 3..5",NEWLINE

string sPlayerXMine, "Player X mine coords (X Y or N): "
string sPlayerOMine, "Player O mine coords (X Y or N): "

string sWrongCoord, "Wrong input, one or two non-negative integers expected",NEWLINE
string sXYWrong, "Wrong input, two non-negative integers expected",NEWLINE
string sXYWrongRange, "That location is out of range.",NEWLINE
string sNWrongRange, "That location is out of range.",NEWLINE

string sGameStarted, "Game started",NEWLINE

string sLeftBorderDebug, "Current board (hex): [ "
string sLeftBorder, "["
string sRightBorder, "]",NEWLINE
string sDot, "."
string sX, "X"
string sO, "O"
string sXMine, "1"
string sXMineHit, "!"
string sOMine, "2"
string sOMineHit, "@"

; To concatinate with action strings
string sPlayerX, "Player X"
string sPlayerO, "Player O"

string sPlaceMine, ", place your mine: "
string sChooseLocation, ", choose your location: "
string sCellNotFree, "That location is already taken.",NEWLINE
string sWins, " wins!", NEWLINE
string sLoses, " found an opponent mine...", NEWLINE


string sFinalBoard, "Final board:",NEWLINE
string sTie, "It's a draw (tie)!", NEWLINE

string inputN, "Treat input as (N)",NEWLINE
string inputXY, "Treat input as (X Y)",NEWLINE

string currXYLeft, "Current XY (hex): ("
string currXYMid, ", "
string currXYRight, ")",NEWLINE

string debugShowMines, "showmines"


section .bss

board resb MAX_BOARD_SIZE * MAX_BOARD_SIZE

boardSize resd 1				; Board dimensions. 3 = 3x3 board, etc
boardLinearSize resd 1			; Board size in memory = boardSize * boardSize

isDebug resd 1					; Set in init(), used to generate additional output
prevDebug resd 1				; Copy isDebug here before reading mine location

ioBuffer resb IO_BUFFER_SIZE	; Buffer for I/O

playerXMineX resd 1
playerXMineY resd 1
playerOMineX resd 1
playerOMineY resd 1

currentX resd 1
currentY resd 1

currentPlayer resb 1
currentOpponent resb 1

currentPlayerName resd 1
currentOpponentName resd 1

printLoopData resd 1
printShowMines resd 1

messageData resd 1


section .text					

global	_start					; Entry point


print_int:						; ECX: string, EDX: length
	mov		eax, SYS_WRITE		; System call number (sys_write)
	mov		ebx, STDOUT			; First argument: file descriptor (stdout == 1)
	int		0x80				; Call kernel
ret

read_int:						; ECX: buffer, EDX: size.
	mov		eax, SYS_READ		; System call number (sys_read)
	mov		ebx, STDIN			; First argument: file descriptor (stdin == 0)
	int		0x80				; Call kernel
ret 

exit_int:						; EBX: exit code
	mov		eax, SYS_EXIT		; ebx: error code
	int		0x80
ret

_start:							; Entry point
	call	init				
	test	eax, eax			; Check for errors returned by init()
	jz		play				; Start playing
	
	easy_print	sExitingProgram
	
	mov 	ebx, 1
	jmp 	exit

play:
	call	play_loop			

	xor		ebx, ebx			; Exit code 0
exit:
	call	exit_int
ret								


init: 							; Initializes game, EAX will contain 0 on success

	; Prompt for debug mode, accepts 'y' or 'Y'. Will continue as normal if neither
	xor		eax, eax			; Initialize as 0, will be changed if there are failures
    mov		dword [isDebug], eax	
    mov		dword [prevDebug], eax
	easy_print	sEnableDebug
	
	mov		ecx, ioBuffer
	mov		edx, IO_BUFFER_SIZE
	call	read_int
	mov		byte [ioBuffer + eax], 0	; Terminate with NULL

	mov		esi, ioBuffer		; Skip spaces in input
	cld							; Clear direction flag
init_skip_spaces:
	lodsb
	test	al, al
	jz		init_end_debug
	cmp		al, ' '
	jle 	init_skip_spaces
	cmp		al, 'Y'				; Compare non-space char
	je		init_set_debug	; Must be Y or y to set isDebug
	cmp		al, 'y'
	je		init_set_debug
	jmp		init_end_debug
	
init_set_debug:
	mov		dword [prevDebug], eax		; Set isDebug later
init_end_debug:

	; Read the board size
repeat_board_size:
	easy_print	sBoardSize
	
	mov		ecx, ioBuffer
	mov		edx, IO_BUFFER_SIZE
	call	read_int
	
	mov		esi, ioBuffer		; Buffer for input
	call	get_number			; Retrieve an integer from input
	cmp		eax, 3				; Board size must be at least 3
	jl		wrong_board_size	; Invalid input
	cmp		eax, MAX_BOARD_SIZE	; Board size must be less than MAX_BOARD_SIZE
	jg		wrong_board_size	; Invalid input
	mov		[boardSize], eax	; boardSize stores dimensions during the game

	; Calculate the linear size for 1-number player inputs
	mul		eax					; EAX = boardSize * boardSize
	mov		[boardLinearSize], eax	; board size 

	; Read the locations of players' mines
	mov		dword [currentPlayerName], sPlayerX	; Player X is current player
	mov		dword [messageData], sPlaceMine 	; Placing mine is the action
	call	get_player_input	; Returns when input is correct
	mov		eax, [currentX]		
	mov		[playerXMineX], eax	; Store X-coord if player X's mine
	mov		eax, [currentY]
	mov		[playerXMineY], eax	; Store Y-coord if player X's mine
	
	mov		bl, [currentX]
	mov		bh, [currentY]
	mov		cl, CELL_MINE_X
	call	set_cell			; Store the mine in the appropriate location
	
	mov		dword [currentPlayerName], sPlayerO	; Player O is the current player
	mov		dword [messageData], sPlaceMine		; Placing mine is the action
	call	get_player_input	; Returns when location is correct
	mov		eax, [currentX]		
	mov		[playerOMineX], eax	; Store X-coord if player O's mine
	mov		eax, [currentY]
	mov		[playerOMineY], eax	; Store Y-coord if player O's mine
	
	mov		bl, [playerOMineX]	; Mines can overlap,
	mov		bh, [playerOMineY]
	call	get_cell			; Get the cell first
	or		eax, CELL_MINE_O	; Add the mine to its current value
	mov		cl, al
	mov		bl, [playerOMineX]
	mov		bh, [playerOMineY]
	call	set_cell			; Store the mine in the appropriate location
	
	mov		eax, [prevDebug]
	mov		[isDebug], eax
	
	xor		eax, eax			; Place 0 in EAX if successful
ret								; Return from init

wrong_board_size:
	easy_print	sWrongBoardSize	; Print wrong board size error
%ifdef WRONG_BOARD_SIZE_ABORTS	; Do not repeat
	mov		ebx, 1
	call	exit_int
%endif
	jmp		repeat_board_size	; Repeat the entire block


play_loop:

	easy_print	sGameStarted

play_main_loop:
	; Player X turn
	xor		eax, eax
	call	print_board			
	
	call	check_tie
	jc		tie
	
	mov		bl, PLAYER_X
	mov		bh, PLAYER_O
	mov		esi, sChooseLocation
	mov		dword [currentPlayerName], sPlayerX
	mov		dword [currentOpponentName], sPlayerO
	call	check_turn

	; Player O turn
	xor		eax, eax
	call	print_board			
	
	call	check_tie
	jc		tie
	
	mov		bl, PLAYER_O
	mov		bh, PLAYER_X
	mov		esi, sChooseLocation
	mov		dword [currentPlayerName], sPlayerO
	mov		dword [currentOpponentName], sPlayerX
	call	check_turn
	
	jmp		play_main_loop
	
	xor		eax, eax			; Should not reach this command
ret								; Should not reach this command
tie:
	easy_print	sTie			; Print tie message
	xor		ebx, ebx
	call	exit_int			; Exit game
ret								; Should not reach this command


; BL: player, in BH: opponent, in ESI: message)
check_turn:
	mov		[currentPlayer], bl
	mov		[currentOpponent], bh
	mov		[messageData], esi
get_player_input_again:
	mov		esi, [messageData]	; Restore ESI
	call	get_player_input	; Message is already in ESI
	
	mov		bl, [currentX]
	mov		bh, [currentY]
	call	get_cell			; EAX stores the cell
	
	cmp		eax, CELL_X
	je		cell_is_taken
	cmp		eax, CELL_O
	je		cell_is_taken
	

	xor		ecx, ecx
	mov		cl, [currentOpponent]
	shl		ecx, 4				; ECX stores opponent's mine
	test	eax, ecx
	jnz		found_opponent_mine
	
	mov		bl, [currentX]
	mov		bh, [currentY]
	mov		cl, [currentPlayer]
	call	set_cell			
	
	; Check for victory
	call	check_victory
	test	eax, eax
	jnz		victory
ret
found_opponent_mine:
	mov		[printShowMines], eax
	easy_print sFinalBoard
	mov		eax, [printShowMines]	; EAX stores opponent's mine
	mov		eax, [currentOpponent]
	shl		eax, 4				; Opponent mine flag
	call	print_board			; Final board, opponent mine exploded
	mov		esi, [currentPlayerName]
	call	print_string
	easy_print sLoses
	mov		esi, [currentOpponentName]
	call	print_string
	easy_print sWins
	xor		ebx, ebx
	call	exit_int
ret
victory:
	easy_print sFinalBoard
	mov		eax, [currentPlayer]	
	call	print_board			; Final board, opponent mine exploded
	mov		esi, [currentPlayerName]
	call	print_string
	easy_print sWins
	xor		ebx, ebx
	call	exit_int
ret
cell_is_taken:
	easy_print sCellNotFree
	jmp		get_player_input_again


check_tie:
	cld							; Clear direction flag
	mov		esi, board
	mov		ecx, [boardLinearSize]
check_tie_loop:
	lodsb
	and		al, CELL_X | CELL_O
	jz		found_free_cell
	loop	check_tie_loop
	stc							; No free cells
ret
found_free_cell:
	clc
ret


check_victory:
%ifdef DIAGONALS_WIN
	mov		eax, [boardSize]
	test	al, 1
	jz		skip_diagonals		; Board size must be odd
	mov		bl, [currentX]
	mov		bh, [currentY]
	cmp		bh, bl				; Check if X = Y
	je		check_diagonals
	add		bh, bl
	mov		bl, [boardSize]
	dec		bl
	cmp		bh, bl				; Check if X + Y = boardSize - 1
	jne		skip_diagonals

check_diagonals:
	mov		ch, [currentX]		
	mov		bl, ch
	mov		bh, [currentY]
	call	get_cell			; Store cell value
	xor		cl, cl				; Counter
	mov		esi, eax			; Column values
	mov		edi, eax			; Row values
diagonal_loop:
	mov		bl, cl				; Check the cell
	mov		bh, cl
	call	get_cell
	and		esi, eax			
	
	mov		bl, [boardSize]		
	sub		bl, cl				; boardSize - i
	dec		bl					; BL = boardSize - i - 1
	mov		bh, cl				
	call	get_cell
	and		edi, eax			
	
	inc		cl
	cmp		cl, [boardSize]
	jl		diagonal_loop
	
	mov		eax, esi
	or		eax, edi
	jnz		victory_checked	
%endif 

skip_diagonals:
	mov		ecx, [boardSize]
	xor		ebx, ebx
	mov		bl, [currentX]
	mov		bh, [currentY]
	call	get_cell
	mov		esi, eax			; Column values
	mov		edi, eax			; Row values
line_loop:						; ECX stores counter for the loop
	dec		ecx 
	
	mov		bl, [currentX]
	mov		bh, cl
	call	get_cell			; Check cell in X column
	and		esi, eax			
	
	mov		bh, [currentY]
	mov		bl, cl
	call	get_cell			; Check cell in Y row
	and		edi, eax			
	
	test	ecx, ecx
	jnz		line_loop
	; If either row or column at (x,y) has all the same values, there is a victory
	mov		eax, esi
	or		eax, edi			; Victory = non-zero value
victory_checked:
ret

get_cell:
	xor		eax, eax
	mov		al, bh				; BH stores Y
	mul		dword [boardSize]	; Y * boardSize
	and		ebx, 0FFh			; EBX stores column
	add		eax, ebx			; Y * boardSize + X
	add		eax, board			; EAX points to board
	mov		eax, [eax]			
	and		eax, 0FFh
ret

set_cell:
	xor		eax, eax
	mov		al, bh
	mul		dword [boardSize]
	and		ebx, 0FFh
	add		eax, ebx
	add		eax, board
	mov		[eax], cl
ret

print_board:
	
	; Preserve showMines
	mov		[printShowMines], eax

	mov		eax, [isDebug]
	test	eax, eax
	jz		no_hex_board
	call	print_hex_board
no_hex_board:
	
	
	xor		ebx, ebx
	mov		[printLoopData], ebx	; Preserve the counter for the loop

row_loop:						; Outer loop
	easy_print	sLeftBorder

	mov		ebx, [printLoopData]	; Restore the counter for the loop, EBX was modified by print_int
	mov		bl, 0               ; BH = [0..boardSize - 1]

col_loop:						; Nested loop

	mov		[printLoopData], ebx	; Preserve the loop counter
	
	; Translate coords to board index
	call	get_cell			; EAX points to cell data
	cmp		eax, CELL_X
	jne		notX
	easy_print	sX
	jmp		completed_char
notX:
	cmp		eax, CELL_O
	jne		notO
	easy_print	sO
	jmp		completed_char
notO:
	; Other possible values depend on showMines feature
	mov		edx, [printShowMines]
	test	edx, edx
	jz		just_draw_dot		; printShowMines = 0
	; printShowMines != 0
	test	edx, CELL_MINE_X	; Draw X mine exploded (!)
	jz		notMineX
	test	eax, CELL_MINE_X	; Check for mine
	jz		notMineO			;just_draw_dot
	easy_print	sXMineHit		; If there is a mine
	jmp		completed_char
notMineX:
	test	edx, CELL_MINE_O	; Draw O mine exploded (@)
	jz		notMineO
	test	eax, CELL_MINE_O	
	jz		notMineO
	easy_print	sOMineHit		; Check for mine
	jmp		completed_char
notMineO:						; Draw mines as 1 and 2 (if not hit)
	test	eax, CELL_MINE_X
	jz		mineO
	easy_print	sXMine
	jmp		completed_char
mineO:
	test	eax, CELL_MINE_O
	jz		just_draw_dot
	easy_print	sOMine
	jmp		completed_char

just_draw_dot:
	easy_print	sDot			; Empty field
completed_char:

	mov		ebx, [printLoopData]	; Restore the loop counter
	inc		bl					; Go to the next col in the current row
	mov		[printLoopData], ebx	; Preserve the loop counter
	cmp		bl, [boardSize]		; Check if the last column has been reached
	jl		col_loop			; There are more columns to print
	
	easy_print	sRightBorder
	
	mov		ebx, [printLoopData]	; Restore the loop counter
	inc		bh					; Go to the next row
	mov		[printLoopData], ebx	; Preserve the loop counter
	cmp		bh, [boardSize]
	jl		row_loop			; There are more rows to print
	
ret


print_hex_board:
	; Print the hex code
	easy_print	sLeftBorderDebug
	xor		eax, eax
	mov		[printLoopData], eax	; counter
hex_board_loop:
	mov		ebx, [printLoopData]
	xor		eax, eax
	mov		al, [board + ebx]
	shr		al, 4					; EAX stores high nibble of the current board char
	mov		al, [HEX + eax]
	mov		[ioBuffer], al          ; Write hex representation to the buffer
	mov		al, [board + ebx]
	and		al, 15					; Low nibble
	mov		al, [HEX + eax]
	mov		[ioBuffer + 1], al		; Append it to the buffer
	mov		byte [ioBuffer + 2], ' '	
	mov		ecx, ioBuffer
	mov		edx, 3
	call	print_int
	
	mov		eax, [printLoopData]
	inc		eax
	mov		[printLoopData], eax
	cmp		eax, [boardLinearSize]
	jne		hex_board_loop
	easy_print	sRightBorder
ret

; ---------------------------------------------------------------------
;	get_player_input()
;	Does not preserve registers
;	Uses messageData (.bss)
;	Sets currentX, currentY (.bss)
; NOTE: get_player_input accepts 3 kind of inputs:
; * Debugging commands (if enabled)
; * Cell coordinates in (X, Y) form
; * Cell coordinates as a single integer N, where 
;	X = N mod boardSize
;	Y = N div boardSize
; (linear offset in board[])

get_player_input:
	; Prompt user for input
	mov		esi, [currentPlayerName]
	call	print_string
	mov		esi, [messageData]
	call	print_string

	; Read input into ioBuffer
	mov		ecx, ioBuffer
	mov		edx, IO_BUFFER_SIZE
	call	read_int			
	mov		byte [ioBuffer + eax], 0
	mov		ebx, eax			; Preserve input length
	
	mov		ecx, [isDebug]
	test	ecx, ecx
	jz		player_input_no_debug
	
	; Compare ioBuffer to known debug commands
	mov		esi, debugShowMines
	mov		edi, ioBuffer
	mov		ecx, debugShowMines_length
	repe	cmpsb				; Break on first non-matching char
	jne		not_debugShowMines
	call	debug_show_mines
	jmp		get_player_input	; Request the input again

not_debugShowMines:
	
player_input_no_debug:

	; ioBuffer may contain 1 or 2 integers
	mov		esi, ioBuffer
	call	get_number
	jc		wrong_player_input 	; First substring is not a number. Wrong input
	mov		[currentX], eax		; Preserve this number in currentX, use it later
	call	get_number			; First substring is a number. Check the second one
	jnc		input_xy_format
	; First substring is a number, and second is not
	mov		al, [esi]			
	test	al, al				; Null terminator
	jne		wrong_player_input
	
	mov		eax, [isDebug]
input_as_n:
	; Treat this as (N) input
	mov		eax, [currentX]		; N was stored in currentX
	cmp		eax, [boardLinearSize]
	jge		wrong_n_format
	xor		edx, edx
	div		dword [boardSize]
	mov		[currentY], eax
	mov		[currentX], edx
	; Correct, can return now!
	mov		eax, [isDebug]
	test	eax, eax			; If isDebug,
	jz		valid_input
	call 	debug_print_xy		; then print(currentX, currentY)

valid_input:
ret
input_xy_format:
	mov		[currentY], eax
	mov		eax, [isDebug]
	test	eax, eax			; If isDebug,
	jz		input_xy_format_quiet
	easy_print	inputXY			; then notify user about the input format
input_xy_format_quiet:
	call	validate_xy
	jnc		valid_input
	easy_print	sXYWrongRange	; X/Y must be in [0..boardSize)
jmp			get_player_input	; Read it again
wrong_player_input:
	easy_print	sWrongCoord		; X/Y must be in [0..boardSize)
jmp			get_player_input
wrong_n_format:
	easy_print	sNWrongRange	; N must be in [0..boardLinearSize)
jmp			get_player_input


debug_print_xy:
	easy_print currXYLeft
	mov 	eax, [currentX]
	call 	print_hex
	easy_print currXYMid
	mov eax, [currentY]
	call print_hex
	easy_print currXYRight
ret

; Shows mines when command 'showmines' is entered
debug_show_mines:
	mov		eax, 1
	call	print_board
ret


print_hex:
	mov edx, eax
	mov ecx, 8
	mov ebx, HEX
	lea esi, [ioBuffer + 9]
	mov byte [esi], 0
print_hex_loop:
	dec esi
	mov al, dl
	and al, 15
	xlat
	shr edx, 4
	mov [esi], al
	test edx, edx
	jz print_hex_end
	loop print_hex_loop
print_hex_end:
	call print_string
ret

validate_xy:
	mov		eax, [boardSize]	; Load it once to compare with both numbers
	cmp		eax, [currentX]
	jle		wrong_xy
	cmp		eax, [currentY]
	jle		wrong_xy
	clc							; Success
ret
wrong_xy:
	stc							; Error condition
ret


; =====================================================================
; I/O helper routines

; ---------------------------------------------------------------------
;    get_number(inout ESI: string) EAX: result, CF: error
;	Modifies: ECX
;
; String at ESI must contain at least one numeric char,
; and must be terminated by space or 0..31 char
; ESI will point to the char after the terminator, and may be reused.
get_number:
	xor 	eax, eax			; EAX holds the current digit
	xor		ecx, ecx			; ECX accumulates digits
trim_left:
	mov		al, [esi]			; AL stores first decimal digit or space
	test	eax, eax			
	je		error				; Preserve ESI 
	inc		esi					; Advance to the next char
	cmp		al, ' '
	jle		trim_left			; Skip all leading space chars and 0..31
read_next:
	sub		al, '0' 			; 30h..39h -> 0..9 in decimal
	cmp		al, 9				; Must be in the range
	jg		error 				; Not a number
	; Multiply ECX by 10
	lea		ecx, [ecx + ecx*4]	; ECX <- ECX * 5   
	shl		ecx, 1				; ECX <- ECX * 2   
	add		ecx, eax
	lodsb						; AL stores next decimal digit
	test	al, al				
	jz		end					; No more numbers
	cmp		al, ' '
	jle		end					; Test for space, tab, other chars in 0..31
	jmp		read_next
end:
	mov		eax, ecx			; Return value in EAX
	clc
ret
error:							; EAX is undefined
	stc							; Error condition
ret

print_string:
	cld
	mov		edi, esi
	xor		eax, eax			; String terminator = 0
	xor		ecx, ecx
	not		ecx					; Bytes to scan
	repnz	scasb				; Find the terminating char
	sub		edi, esi			; Number of bytes to print
	mov		ecx, esi
	mov		edx, edi
	dec		edx
	call	print_int
ret


