;myIOcalc.asm
;Diana Bell
;CMSC 313 Project 2

section .data 
msg db "Sum is: "
len equ $ - msg
eol db 0xA, 0xD
eol_len equ $ - eol

section .bss
sum resb 2
num1 resb 2
num2 resb 2

section .text
global _start
print_int:
mov eax, 4 ;defining routine print_int
mov ebx, 1 ;file descriptor (stdout)
int 0x80 ;system call number(sys_write)
ret ;return back

_start: 
;Read and store first user input
mov eax, 3 ;system call for read
mov ebx, 1 ;read what the user input
mov ecx, num1
mov edx, 2 ;2 bytes of the input
int 80h
;Read and store second input
mov eax, 3 ;system call for read
mov ebx, 1 ;read what the user input
mov ecx, num2 
mov edx, 2 ;2 bytes of the input
int 80h
;Add num 1 and 2 and save it in sum
mov eax, [num1]
sub eax, '0'
mov ebx, [num2]
sub ebx, '0'
add eax, ebx
add eax, '0'
mov [sum], eax
;Print result
mov ecx, msg
mov edx, len
call print_int
mov ecx, sum
mov edx, 2
call print_int
mov ecx, eol
mov edx, eol_len
call print_int
mov eax, 1 ;system call number (sys_exit)
xor ebx, ebx
int 0x80 
